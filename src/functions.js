import React from "react";
import moment from "moment";

const icons = {
    asc: <i className="fa fa-arrow-up" aria-hidden="true"/>,
    desc: <i className="fa fa-arrow-down" aria-hidden="true"/>
};
const months = [
    'января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
    'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
];

export default {
    getIcon: type => {
        return icons[type]
    },

    formatDate: date => {
        let formatDate = moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD');

        if (formatDate === 'Invalid date') {
            const arr = date.split(' ');
            const index = months.indexOf(arr[1]) + 1;

            if (index !== -1) {
                arr[1] = index.length > 1 ? index : '0' + index;
            }

            formatDate = moment(arr.join('/'), 'DD/MM/YYYY').format('YYYY-MM-DD');
        }

        return formatDate
    }
}
import React, { Component } from 'react';
import {Router, Route, IndexRoute} from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import PageTable from "./components/PageTable";
import PageMain from "./components/PageMain";

import "./App.css";


export default class Root extends Component {
    render() {
        return (
            <Router history={createBrowserHistory()}>
                <Route path='/' component={App}>
                    <IndexRoute component={PageMain}/>
                    <Route path='table' component={PageTable}/>
                </Route>
            </Router>
        );
    }
}

class App extends Component {
    render() {
        return (
            <div className="app">
                {this.props.children}
            </div>
        );
    }
}

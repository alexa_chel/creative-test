import React, {Component} from 'react';
import {Link} from "react-router";
import Table from "./Table";

import "./PageTable.css";

class PageTable extends Component {
    state = {
        searchValue: '',
        data: [
            {id: 1, name: "Вася", date: "15.06.2018", count: 11},
            {id: 2, name: "Петя", date: "23.11.2018", count: 23},
            {id: 3, name: "Иван", date: "12 марта 2017", count: 3},
            {id: 4, name: "Александр", date: "20/12/2010", count: 1},
            {id: 5, name: "Евгений", date: "12.09.2018", count: 112},
            {id: 6, name: "Мария", date: "1.08.2016", count: 122},
            {id: 7, name: "Анастасия", date: "20.11.2018", count: 34},
            {id: 8, name: "Степан", date: "12.11.2019", count: 10},
        ]
    };

    render() {
        const {data, searchValue} = this.state;

        return (
            <div className="page-table">
                <input className="page-table__input-search" placeholder="Search"
                       onChange={e => this.setState({searchValue: e.target.value})}/>
                <Table data={data} search={searchValue}/>
                <Link to="/" className="page-table__link">Назад</Link>
            </div>
        );
    }
}

export default PageTable
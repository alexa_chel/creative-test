import React, {Component} from 'react';
import {Link} from "react-router";

import "./PageMain.css";
import Modal from "./Modal";

class PageMain extends Component {
    state = {
        isOpenModal: false
    };

    static get modalBody() {
        return <div className="modal-body">
            Hello, world!
        </div>
    }

    render() {
        const {isOpenModal} = this.state;

        return (
            <div className="page-main">
                <a className="page-main__button" href="#"
                   onClick={() => this.setState({isOpenModal: true})}>
                    Task 1
                </a>
                <Link to='/table' className="page-main__button">Task 2</Link>
                <Modal isOpen={isOpenModal} body={PageMain.modalBody} onCloseModal={() => this.setState({isOpenModal: false})}/>
            </div>
        );
    }
}

export default PageMain
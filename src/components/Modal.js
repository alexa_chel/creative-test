import React, {Component} from 'react';
import * as ReactDOM from "react-dom";
import PropTypes from 'prop-types';

import "./Modal.css";

const modal = document.getElementById('modal');

class Modal extends Component {
    constructor(props) {
        super(props);

        this.container = document.createElement('div');
    }

    componentDidMount() {
        modal.appendChild(this.container);
    }

    componentWillUnmount() {
        modal.removeChild(this.container);
    }

    onCloseModal = e => {
        if (this.bgModal === e.target) {
            this.props.onCloseModal();
        }
    };

    render() {
        const {isOpen, body} = this.props;

        return isOpen ? ReactDOM.createPortal(
            <div ref={bgModal => this.bgModal = bgModal}
                 className="modal-container"
                 onClick={e => this.onCloseModal(e)}>
                {body}
            </div>,
            this.container,
        ) : null;
    }
}

Modal.propTypes = {
    isOpen: PropTypes.bool,
    onCloseModal: PropTypes.func
};

Modal.defaultProps = {
    isOpen: false
};

export default Modal
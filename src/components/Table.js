import React, {Component} from 'react';
import PropTypes from 'prop-types';
import functions from "../functions";

import "./Table.css";

class Table extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedColumn: undefined,
            sortedData: undefined,
            orderBy: 'default'
        };
    }

    onClickColumn = column => {
        const {data} = this.props;
        const {selectedColumn, orderBy} = this.state;
        let orderByNew = 'asc';
        let sortedData = [];

        if (selectedColumn === column) {
            switch (orderBy) {
                case 'asc':
                    orderByNew = 'desc';
                    break;
                case 'desc':
                    orderByNew = 'default';
                    break;
                default:
                    orderByNew = 'asc';
            }
        }

        if (orderByNew === 'asc') {
            sortedData = data.slice().sort((a, b) => this.sortElements(a, b, column))
        } else if (orderByNew === 'desc') {
            sortedData = data.slice().sort((a, b) => this.sortElements(a, b, column)).reverse()
        } else {
            sortedData = data
        }

        this.setState({
            selectedColumn: column,
            orderBy: orderByNew,
            sortedData
        });
    };

    sortElements = (a, b, column) => {
        if (column !== 'date') {
            if (a[column] < b[column])
                return -1;
            if (a[column] > b[column])
                return 1;
        } else {
            if (new Date(functions.formatDate(a[column])) < new Date(functions.formatDate(b[column])))
                return -1;
            if (new Date(functions.formatDate(a[column])) > new Date(functions.formatDate(b[column])))
                return 1;
        }
        return 0;
    };

    render() {
        const {data, search} = this.props;
        const {selectedColumn, sortedData, orderBy} = this.state;
        const columns = data ? Object.keys(data[0]) : [];
        const filterData = selectedColumn && search ? (sortedData || data).filter(item => {
            return item[selectedColumn].toString().toLowerCase().indexOf(search.toLowerCase()) !== -1
        }) : sortedData || data;

        return (
            <table className="sort-table">
                <thead className="sort-table__thead">
                <tr className="sort-table__tr">
                    {
                        columns.map(col =>
                            <td key={col} className={`sort-table__td ${selectedColumn === col ?
                                'sort-table__td-selected' : ''}`}
                                onClick={() => this.onClickColumn(col)}>
                                {col}
                                <div className="sort-table__thead-icon">
                                    {
                                        selectedColumn === col ?
                                            functions.getIcon(orderBy) : null
                                    }
                                </div>
                            </td>
                        )
                    }
                </tr>
                </thead>
                <tbody className="sort-table__tbody">
                {
                    filterData.map((item, key) => {
                        return <tr key={key} className="sort-table__tr">
                            {
                                columns.map(col =>
                                    <td key={col} className={`sort-table__td ${selectedColumn === col ?
                                        'sort-table__td-selected' : ''}`}>
                                        {col !== 'date' ? item[col] : functions.formatDate(item[col])}
                                    </td>
                                )
                            }
                        </tr>
                    })
                }
                </tbody>
            </table>
        );
    }
}

Table.propTypes = {
    data: PropTypes.array,
    search: PropTypes.string
};

Table.defaultProps = {
    data: [],
    search: ''
};

export default Table